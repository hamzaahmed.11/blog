<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Post;
use App\Models\Tag;
use App\Utils\AppGlobal;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Redirect;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::fetchRecords(['orderBy'=>'id','sort'=>'desc','limit'=>AppGlobal::LIMIT]);

        return view('home.index', compact('posts'));
    }

    public function cities()
    {
        $cities = City::all();

        return view('home.cities', compact('cities'));
    }

    public function tags()
    {
        $tags = Tag::all();

        return view('home.tags', compact('tags'));
    }

    public function autocomplete()
    {
        $cities = City::all();

        return $cities->toJson();
    }

    public function search(Request $request)
    {
        $validated = $request->validate([
            'search' => 'required'
        ]);

        if($validated)
        {
            $error = false;
            $query = $request->input('search');
            $posts = Post::fetchRecords(['whereKey'=>'title','whereVal'=>$query,'orderBy'=>'id','sort'=>'desc','limit'=>AppGlobal::LIMIT]);
        }
        else
        {
            $error = true;
            $query = null;
            $posts = null;
        }

        return view('home.search', compact('error', 'query','posts'));
    }

    public function post($id)
    {
        $post = Post::find($id);

        return view('home.post', compact('post'));
    }

    public function city($name)
    {
        $city = City::where('name',$name)->first();
        $posts = $city->posts()->paginate(AppGlobal::LIMIT);

        return view('home.city', compact('city', 'posts'));
    }

    public function tag($name)
    {
        $tag = Tag::where('name',$name)->first();
        $posts = $tag->posts()->paginate(AppGlobal::LIMIT);

        return view('home.tag', compact('tag', 'posts'));
    }
}
