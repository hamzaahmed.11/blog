<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use App\Utils\AppGlobal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function posts()
    {
        $all = Auth::user()->posts();

        $posts = $all->orderBy('id','desc')->paginate(AppGlobal::LIMIT);

        return view('user.posts', compact('posts'));
    }

    public function post_data($id = null)
    {
        $tags = Tag::all();

        if(!$id)
        {
            $title = 'New Post';
            $post = null;
        }
        else
        {
            $title = 'Edit Post';
            $post = Post::find($id);
        }

        return view('user.post', compact('title', 'post','tags'));
    }

    public function new_post(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validated)
        {
            $file = $request->file('image');

            if($file)
            {
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $file_path = 'assets/images/uploads/';
                $file->move($file_path, $file_name);

                $file_name = 'uploads/'.$file_name;
            }
            else
            {
                $file_name = 'default.png';
            }

            $post = Post::create([
                'user_id' => Auth::user()->id,
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'image' => $file_name
            ]);

            $tags = $request->input('tags');

            if($tags)
            {
                $post->tags()->sync($request->input('tags'));
            }

            return redirect()->route('posts');
        }
    }

    public function edit_post(Request $request, $id)
    {
        $validated = $request->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        if($validated)
        {
            $file = $request->file('image');

            if($file)
            {
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $file_path = 'assets/images/uploads/';
                $file->move($file_path, $file_name);

                $file_name = 'uploads/'.$file_name;
            }
            else
            {
                $file_name = $request->input('prevFile');
            }

            $post = Post::where('id',$id)->update([
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'image' => $file_name
            ]);

            $tags = $request->input('tags');

            if($tags)
            {
                $post->tags()->sync($request->input('tags'));
            }

            return redirect()->route('posts');
        }
    }

    public function delete($id)
    {
        $post = Post::find($id);
        if($post->image != "default.png")
        {
            File::delete(public_path('assets/images/') . $post->image);
        }
        $post->delete();

        return redirect()->route('posts');
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::to('home');
    }
}
