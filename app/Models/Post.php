<?php

namespace App\Models;

use App\Utils\AppGlobal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'title', 'description', 'image',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getCityAttribute()
    {
        return $this->user->city->name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public static function fetchRecords(array $filters = null)
    {
        $orderBy = isset($filters['orderBy']) ? $filters['orderBy'] : 'id';
        $sort = isset($filters['sort']) ? $filters['sort'] : 'desc';
        $limit = isset($filters['limit']) ? $filters['limit'] : AppGlobal::LIMIT;

        $whereKey = isset($filters['whereKey']) ? $filters['whereKey'] : null;
        $whereVal = isset($filters['whereVal']) ? $filters['whereVal'] : null;

        if($whereKey && $whereVal)
        {
            $posts = DB::table('posts')->where($whereKey, 'LIKE', '%'.$whereVal.'%')->orderBy($orderBy, $sort)->paginate($limit);
        }
        else
        {
            $posts = DB::table('posts')->orderBy($orderBy, $sort)->paginate($limit);
        }

        return $posts;
    }
}
