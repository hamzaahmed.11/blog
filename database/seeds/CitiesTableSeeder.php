<?php

use App\Models\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ["name" => "Karachi"],["name" => "Lahore"],["name" => "Faisalābād"],["name" => "Serai"],["name" => "Rāwalpindi"],["name" => "Multān"],["name" => "Gujrānwāla"],["name" => "Hyderābād City"],["name" => "Peshāwar"],["name" => "Abbottābād"],["name" => "Islamabad"],["name" => "Quetta"],["name" => "Bannu"],["name" => "Bahāwalpur"],["name" => "Sargodha"],["name" => "Siālkot City"],["name" => "Sukkur"],["name" => "Lārkāna"],["name" => "Sheikhupura"],["name" => "Mīrpur Khās"],["name" => "Rahīmyār Khān"],["name" => "Kohāt"],["name" => "Jhang Sadr"],["name" => "Gujrāt"],["name" => "Bardār"],["name" => "Kasūr"],["name" => "Dera Ghāzi Khān"],["name" => "Masīwāla"],["name" => "Nawābshāh"],["name" => "Okāra"],["name" => "Gilgit"],["name" => "Chiniot"],["name" => "Sādiqābād"],["name" => "Turbat"],["name" => "Dera Ismāīl Khān"],["name" => "Chaman"],["name" => "Zhob"],["name" => "Mehra"],["name" => "Parachinār"],["name" => "Gwādar"],["name" => "Kundiān"],["name" => "Shahdād Kot"],["name" => "Harīpur"],["name" => "Matiāri"],["name" => "Dera Allāhyār"],["name" => "Lodhrān"],["name" => "Batgrām"],["name" => "Thatta"],["name" => "Bāgh"],["name" => "Badīn"],["name" => "Mānsehra"],["name" => "Ziārat"],["name" => "Muzaffargarh"],["name" => "Tando Allāhyār"],["name" => "Dera Murād Jamāli"],["name" => "Karak"],["name" => "Mardan"],["name" => "Uthal"],["name" => "Nankāna Sāhib"],["name" => "Bārkhān"],["name" => "Hāfizābād"],["name" => "Kotli"],["name" => "Loralai"],["name" => "Dera Bugti"],["name" => "Jhang City"],["name" => "Sāhīwāl"],["name" => "Sānghar"],["name" => "Pākpattan"],["name" => "Chakwāl"],["name" => "Khushāb"],["name" => "Ghotki"],["name" => "Kohlu"],["name" => "Khuzdār"],["name" => "Awārān"],["name" => "Nowshera"],["name" => "Chārsadda"],["name" => "Qila Abdullāh"],["name" => "Bahāwalnagar"],["name" => "Dādu"],["name" => "Alīābad"],["name" => "Lakki Marwat"],["name" => "Chilās"],["name" => "Pishin"],["name" => "Tānk"],["name" => "Chitrāl"],["name" => "Qila Saifullāh"],["name" => "Shikārpur"],["name" => "Panjgūr"],["name" => "Mastung"],["name" => "Kalāt"],["name" => "Gandāvā"],["name" => "Khānewāl"],["name" => "Nārowāl"],["name" => "Khairpur"],["name" => "Malakand"],["name" => "Vihāri"],["name" => "Saidu Sharif"],["name" => "Jhelum"],["name" => "Mandi Bahāuddīn"],["name" => "Bhakkar"],["name" => "Toba Tek Singh"],["name" => "Jāmshoro"],["name" => "Khārān"],["name" => "Umarkot"],["name" => "Hangu"],["name" => "Timargara"],["name" => "Gākuch"],["name" => "Jacobābād"],["name" => "Alpūrai"],["name" => "Miānwāli"],["name" => "Mūsa Khel Bāzār"],["name" => "Naushahro Fīroz"],["name" => "New Mīrpur"],["name" => "Daggar"],["name" => "Eidgāh"],["name" => "Sibi"],["name" => "Dālbandīn"],["name" => "Rājanpur"],["name" => "Leiah"],["name" => "Upper Dir"],["name" => "Tando Muhammad Khān"],["name" => "Attock City"],["name" => "Rāwala Kot"],["name" => "Swābi"],["name" => "Kandhkot"],["name" => "Dasu"],["name" => "Athmuqam"],
        ];

        foreach ($rows as $row)
        {
            City::create($row);
        }
    }
}
