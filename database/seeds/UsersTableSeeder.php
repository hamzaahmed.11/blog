<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            ['name' => 'Hamza', 'email' => 'hamza@gmail.com', 'password' => Hash::make('hamza12345'), 'city_id' => 1],
            ['name' => 'Ali', 'email' => 'ali@gmail.com', 'password' => Hash::make('ali12345'), 'city_id' => 2],
        ];

        foreach ($rows as $row)
        {
            User::create($row);
        }
    }
}
