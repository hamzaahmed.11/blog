@extends('admin.layouts.main')

@section('title', 'Admin')

@section('container')
    <div class="container mt-3 mb-2">

        @if($users != null && count($users) > 0)
            <div class="row">
                <div class="card">
                    <div class="card-header d-flex pr-0">
                        <h5 class="card-title mb-0">Users</h5>
                    </div>
                    <table class="table mb-0 p-1 table-responsive">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">City</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->city->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        @else
            <h4 class="mx-auto mt-5 text-center text-black-50">There are no users.</h4>
        @endif

    </div>
@endsection