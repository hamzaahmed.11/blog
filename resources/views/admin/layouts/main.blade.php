<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/material.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <!-- Styles -->
    @yield('style')
</head>
<body>

<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-primary">
    <a class="navbar-brand mx-auto" href="{{route('home')}}">
        <img src="{{ asset('assets/images/logo.png') }}" width="30" height="30" class="d-inline-block align-top mr-1" alt="">
        <strong>Blog Admin</strong>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <i class="material-icons">menu</i>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mx-auto justify-content-center">
            <li class="nav-item {{ $nav_home ?? '' }}">
                <a class="nav-link" href="{{route('home')}}">Home</a>
            </li>
        </ul>
        <ul class="navbar-nav justify-content-center ml-lg-2 mt-2 mt-lg-0">
            @if (Auth::user())
                <li class="nav-item ml-1">
                    <a class="nav-link btn btn-dark text-capitalize" href="{{ route('admin.auth.logout') }}">Logout</a>
                </li>
            @endif
        </ul>
    </div>
</nav>

<section>
    <div id="mainContainer">
        @section('container')
            <div class="container">
                <h4 class="mx-auto mt-5 text-center text-black-50">There are no posts available.</h4>
            </div>
        @show
    </div>
</section>

<footer class="bg-dark text-white-50">
    <p class="text-center pt-3">&copy; Copyright {{ date('Y') }}. All rights reserved.</p>
</footer>

<!-- Javascript -->
<script src="{{ asset("assets/js/jquery-3.3.1.min.js") }}"></script>
<script src="{{ asset("assets/js/popper.min.js") }}"></script>
<script src="{{ asset("assets/js/bootstrap.min.js") }}"></script>
<script src="{{ asset("assets/js/material.min.js") }}"></script>

@yield('script')
</body>
</html>
