@php($nav_cities = 'active')

@extends('layouts.main')

@section('title', 'Cities')

@section('container')
    <div class="container mt-3 mb-2">

        @if($cities != null && count($cities) > 0)
            <h3 class="text-center shadow-4 pt-2 pb-2 mb-3"><strong>Cities</strong></h3>
            <div class="text-center shadow-4 pt-2 pb-2 mb-3">
                @foreach($cities as $city)
                    <a class="chip chip-action m-1" href="{{ route('city', [$city->name]) }}">{{ $city->name }}</a>
                @endforeach
            </div>
        @else
            <h4 class="mx-auto mt-5 text-center text-black-50">There are no cities available.</h4>
        @endif

    </div>
@endsection