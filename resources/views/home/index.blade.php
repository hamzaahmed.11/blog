@php($nav_home = 'active')

@extends('layouts.main')

@section('title', 'Home')

@section('container')
    <div class="container mt-3 mb-2">

        @if($posts != null && count($posts) > 0)
            <div class="row">
                @foreach($posts as $post)
                    <div class="col-md-6 mb-3">
                        <div class="card">
                            <img alt="{{ $post->title }}" class="card-img-top" src="{{asset('assets/images') . '/' . $post->image}}" style="max-height: 300px">
                            <div class="card-body">
                                <h4 class="card-title">{{ $post->title }}</h4>
{{--                                <p class="card-text">{{ $post->description }}</p>--}}
                                <p class="card-text">{{ strlen($post->description) > 80 ? substr($post->description, 0, 80) . '...' : $post->description }}</p>
                            </div>
                            <div class="card-actions">
                                <a class="btn btn-outline-primary" href="{{ route('post', [$post->id]) }}">Read more</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            {{ $posts->links('layouts.pagination') }}
        @else
            <h4 class="mx-auto mt-5 text-center text-black-50">There are no posts available.</h4>
        @endif

    </div>
@endsection