@php($nav_home = 'active')

@extends('layouts.main')

@section('title', ($post->title) ? $post->title : 'Blog')

@section('container')
    <div class="container mt-3 mb-2">

        @if($post != null)
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="card">
                        <img alt="{{ $post->title }}" class="card-img-top" src="{{asset('assets/images') . '/' . $post->image}}" style="max-height: 300px">
                        <div class="card-body">
                            <p class="card-text">
                                Posted By {{ $post->user->name }}
                                &nbsp;&nbsp;&nbsp;<small>{{ $post->created_at->diffForHumans() }}</small>
                                <a class="btn page-link float-right" href="{{ route('city', [$post->city]) }}"><strong>{{ $post->city }}</strong></a>
                            </p>
                            <div class="clearfix"></div>
                            <div class="border-top"><br></div>
                            <h4 class="card-title">{{ $post->title }}</h4>
                            {{--                                <p class="card-text">{{ $post->description }}</p>--}}
                            <p class="card-text">{{ $post->description }}</p>
                        </div>
                        <div class="border-top"></div>
                        <div class="card-footer">
                            @foreach($post->tags as $tag)
                                <a class="chip chip-action m-1" href="{{ route('tag', [$tag->name]) }}">{{ $tag->name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        @else
            <h4 class="mx-auto mt-5 text-center text-black-50">Post not found.</h4>
        @endif

    </div>
@endsection