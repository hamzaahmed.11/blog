@php($nav_tags = 'active')

@extends('layouts.main')

@section('title', 'Tags')

@section('container')
    <div class="container mt-3 mb-2">

        @if($tags != null && count($tags) > 0)
            <h3 class="text-center shadow-4 pt-2 pb-2 mb-3"><strong>Tags</strong></h3>
            <div class="text-center shadow-4 pt-2 pb-2 mb-3">
                @foreach($tags as $tag)
                    <a class="chip chip-action m-1" href="{{ route('tag', [$tag->name]) }}">{{ $tag->name }}</a>
                @endforeach
            </div>
        @else
            <h4 class="mx-auto mt-5 text-center text-black-50">There are no tags available.</h4>
        @endif

    </div>
@endsection