@extends('layouts.main')

@section('title', 'Posts')

@section('container')
    <div class="container mt-3 mb-2">

        @if($posts != null && count($posts) > 0)
            <div class="row">
                <div class="card">
                    <div class="card-header d-flex pr-0">
                        <h5 class="card-title mb-0">Posts</h5>
                        <div class="card-actions ml-auto py-0">
                            <a class="btn my-0" href="{{ route('new') }}"><i class="material-icons">add_box</i> New Post</a>
                        </div>
                    </div>
                    <table class="table mb-0 p-1 table-responsive">
                        <thead>
                        <tr>
                            <th scope="col" style="width: 15%;">Image</th>
                            <th scope="col" style="width: 25%;">Title</th>
                            <th scope="col" style="width: 45%;">Description</th>
                            <th scope="col" style="width: 15%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td class="p-1"><img alt="{{ $post->title }}" class="img-thumbnail w-100" src="{{asset('assets/images') . '/' . $post->image}}" ></td>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->description }}</td>
                                <td>
                                    <a class="btn-link" href="{{ route('post', [$post->id]) }}" target="new" title="View"><i class="material-icons">open_in_new</i></a>&nbsp;
                                    <a class="btn-link" href="{{ route('edit', [$post->id]) }}" title="Edit"><i class="material-icons">edit</i></a>&nbsp;
                                    <a class="btn-link" href="{{ route('delete', [$post->id]) }}" title="Delete"><i class="material-icons">delete</i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <hr class="my-0 w-100">
                    <div class="card-actions align-items-center justify-content-end">
                        {{ $posts->links('layouts.pagination') }}
                    </div>
                </div>
            </div>
        @else
            <h4 class="mx-auto mt-5 text-center text-black-50">There are no posts available.</h4>
        @endif

    </div>
@endsection