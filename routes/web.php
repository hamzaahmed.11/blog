<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['Public'], function ()
{
    Route::group(['Home'], function ()
    {
        Route::get('/', 'HomeController@index')->name('home');
        Route::get('/home', 'HomeController@index');
        Route::get('/cities', 'HomeController@cities')->name('cities');
        Route::get('/tags', 'HomeController@tags')->name('tags');
        Route::get('/autocomplete', 'HomeController@autocomplete')->name('autocomplete');
        Route::get('/post/{id}', 'HomeController@post')->name('post');
        Route::get('/city/{name}', 'HomeController@city')->name('city');
        Route::get('/tag/{name}', 'HomeController@tag')->name('tag');

        Route::post('/search', 'HomeController@search')->name('search');
    });
});

Route::group(['Private','middleware'=>'auth'], function()
{
    Route::get('/posts', 'UserController@posts')->name('posts');
    Route::get('/new/post', 'UserController@post_data')->name('new');
    Route::post('/new/post', 'UserController@new_post')->name('newPost');
    Route::get('/edit/post/{id}', 'UserController@post_data')->name('edit');
    Route::post('/edit/post/{id}', 'UserController@edit_post')->name('editPost');
    Route::get('/detele/{id}', 'UserController@delete')->name('delete');
    Route::get('/logout', 'UserController@logout')->name('logout');
});

Route::group(['Admin'], function ()
{
    Route::prefix('admin')->group(function () {
        Route::get('/', 'AdminController@index')->name('admin.home');
        Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');
        Route::get('login', 'Auth\AdminLoginController@login')->name('admin.auth.login');
        Route::post('login', 'Auth\AdminLoginController@loginAdmin')->name('admin.auth.loginAdmin');
        Route::get('logout', 'Auth\AdminLoginController@logout')->name('admin.auth.logout');
    });
});
